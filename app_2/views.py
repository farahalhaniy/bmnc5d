from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .forms import *
from django.db import connection
from django.contrib import messages
# Create your views here.
response={}

def register_page(request):
    response['cek'] = "Halaman register"
    response['reg_form'] = RegisterForm
    return render(request, 'app_2/register.html', response)

def hasNumbers(inputString):
	return any(char.isdigit() for char in inputString)

def hasLetter(inputString):
    return any(c.isalpha() for c in inputString)

@csrf_exempt
def validate_pwd(request):
    if request.method == "POST":
        password = request.POST.get('password', False)
        notif = ""
        list = []
        length = len(password)
        if (length<8):
            notif = "Panjang password minimal 8 karakter"
            list.append(notif)
        if (hasNumbers(password)==False):
            notif = "Password harus mengandung angka"
            list.append(notif)
        if (hasLetter(password)==False):
            notif="Password harus mengandung huruf"
            list.append(notif)
        print(list)
        data = {
            'list_of_notif' : list
        }
        return JsonResponse(data)


@csrf_exempt
def check_username(request):
    if request.method == 'POST':
        username = request.POST.get('username', False)
        cursor = connection.cursor()
        cursor.execute('select count(*) from narasumber')
        count = cursor.fetchone()[0] #(2,)
        taken = False
        if(count>0):
            cursor.execute('select username from narasumber')
            username_users = cursor.fetchall()#(delfa,)(salvi,)
            print(username_users[0][0])
            for p in username_users:
                print(str(p[0])+" dan "+str(username))
                if p[0] == username:
                    taken = True
                    break
                else:
                    taken = False
        else:
            taken = False
        data = {
            'uname_is_taken' : taken
        }
        return JsonResponse(data)

@csrf_exempt
def check_npm(request):
    if request.method == 'POST':
        nomor_identitas = request.POST.get('npm', False)
        role = request.POST.get('role', False)
        cursor = connection.cursor()
        taken = False
        no_role = False
        if role == 'dosen':
            cursor.execute('select count(*) from dosen')
            count = cursor.fetchone()[0]
            if(count>0):
                print("cek nik dosen")
                cursor.execute('select nik_dosen from dosen')
                cek_npm = cursor.fetchall()
                print(cek_npm[0][0])
                for p in cek_npm:
                    print(str(p[0])+" dan "+str(nomor_identitas))
                    if p[0] == nomor_identitas:
                        taken = True
                        break
                    else:
                        taken = False
            else:
                taken = False
        elif role == 'mahasiswa':
            cursor.execute('select count(*) from mahasiswa')
            count = cursor.fetchone()[0]
            if(count>0):
                print("cek nik dosen")
                cursor.execute('select npm from mahasiswa')
                cek_npm = cursor.fetchall()
                print(cek_npm[0][0])
                for p in cek_npm:
                    print(str(p[0])+" dan "+str(nomor_identitas))
                    if p[0] == nomor_identitas:
                        taken = True
                        break
                    else:
                        taken = False
            else:
                taken = False

        elif role == 'staf':
            cursor.execute('select count(*) from staf')
            count = cursor.fetchone()[0]
            if(count>0):
                print("cek nik dosen")
                cursor.execute('select nik_staf from staf')
                cek_npm = cursor.fetchall()
                print(cek_npm[0][0])
                for p in cek_npm:
                    print(str(p[0])+" dan "+str(nomor_identitas))
                    if p[0] == nomor_identitas:
                        taken = True
                        break
                    else:
                        taken = False
            else:
                taken = False
        else:
            no_role = True
    data = {
        'uname_is_taken' : taken,
        'cek_role' : no_role
    }
    return JsonResponse(data)

@csrf_exempt
def register_narasumber(request):
    print("masuk form")
    form = RegisterForm(data=request.POST)
    print("hai")
    print(request.method)
    print(form.is_valid())
    print(form.errors)
    if (request.method == 'POST' and form.is_valid()):
        print("hai juga")
        cursor = connection.cursor()
        cursor.execute('select count(*) from narasumber')
        count = cursor.fetchone()[0] #(2,)
        id_narasumber = 0
        if count > 0:
            id_narasumber = count + 1
        else:
            id_narasumber = 1
        username = request.POST.get('username', False)#request.POST.get['username']
        password = request.POST.get('password', False)
        nama = request.POST.get('nama', False)
        role = request.POST.get('role', False)
        nomor_identitas = request.POST.get('nomor_identitas', False)
        email = request.POST.get('email', False)
        no_hp = request.POST.get('no_hp', False)
        tempat = request.POST.get('tempat', False)
        tanggal = request.POST.get('tanggal', False)
        status = request.POST.get('status', False)
        jurusan = request.POST.get('jurusan', False)
        posisi = request.POST.get('posisi', False)
        print(status)
        id_universitas = request.POST.get('id_universitas', False)

        query_mhs_lg = "insert into MAHASISWA (id_narasumber,npm,status) values ("+str(id_narasumber)+", "+"'"+str(nomor_identitas)+"'"+", "+"'"+str(status)+"')"
        query_dosen = "insert into DOSEN (id_narasumber,nik_dosen,jurusan) values ("+str(id_narasumber)+", "+"'"+str(nomor_identitas)+"'"+", "+"'"+str(jurusan)+"')"
        query_staf = "insert into STAF (id_narasumber,nik_staf,posisi) values ("+str(id_narasumber)+", "+"'"+str(nomor_identitas)+"'"+", "+"'"+str(posisi)+"')"
        print(query_mhs_lg)
        print(query_dosen)
        print(query_staf)

        if role == 'dosen' or role == 'staf' or role == 'mahasiswa':
            print(id_universitas)
            query = "insert into NARASUMBER (id, nama, email, tempat, tanggal, no_hp, id_universitas, username, password) values ("+str(id_narasumber)+", "+"'"+str(nama)+"'"+", "+"'"+str(email)+"'"+", "+"'"+str(tempat)+"'"+", "+"'"+str(tanggal)+"'"+", "+"'"+str(no_hp)+"'"+", "+str(id_universitas)+", "+"'"+str(username)+"'"+", "+"'"+str(password)+"')"
            print(query)
            cursor.execute(query)

        if role == 'mahasiswa':
            cursor.execute(query_mhs_lg)
        elif role == 'dosen':
            cursor.execute(query_dosen)
        elif role == 'staf':
            cursor.execute(query_staf)
        messages.success(request, "Selamat! Anda berhasil mendaftar! Silakan login kembali untuk melanjutkan")
        return HttpResponseRedirect(reverse('apps-login:login'))
    else:
        return HttpResponse("null")