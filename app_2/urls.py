from django.conf.urls import url
from .views import *
#url for app
urlpatterns = [
    url(r'^$', register_page, name='register'),
    url(r'^register/submit/$', register_narasumber, name='register-narasumber'),
    url(r'^validate-username/$', check_username, name='validate-username'),
    url(r'^validate-npm/$', check_npm, name='validate-npm'),
    url(r'^validate-pwd/$', validate_pwd, name='validate-pwd'),
]