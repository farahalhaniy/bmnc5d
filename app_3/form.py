from django import forms
from .models import *


class BeritaForm(forms.Form):
	Judul = {
		    'type': 'text',
	        'class': 'form-control',
	        'placeholder': "Insert title for your news",
	}

	Url = {
			'type': 'text',
			'class': 'form-control',
			'placeholder': "Insert url",
	}

	Topikberita = {
		    'type': 'text',
	        'class': 'form-control',
	        'placeholder': "What is the topic for your news?",
	}

	Jumlahkataberita = {
		    'type': 'text',
	        'class': 'form-control',
	        'placeholder': "Number of words in your news",
	}

	Tagberita = {
		    'type': 'text',
	        'class': 'form-control',
	        'placeholder': "Ex. tag1, tag2, tag3, etc",
	}

	Judul =  forms.CharField(label='Judul Berita', required=True, max_length=50, widget=forms.TextInput(attrs=Judul))
	Url =  forms.CharField(label='Url Berita', required=True, max_length=50, widget=forms.TextInput(attrs=Url))
	Topik =  forms.CharField(label='Topic', required=True, max_length=200, widget=forms.TextInput(attrs=Topikberita))
	JumlahKata =  forms.CharField(label='Number of Words', required=True, max_length=10, widget=forms.TextInput(attrs=Jumlahkataberita))
	Tag =  forms.CharField(label='Tags', required=True, max_length=200, widget=forms.TextInput(attrs=Tagberita))
