from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .form import *
from django.db import connection
from django.contrib import messages
from datetime import datetime


response = {}

# Create your views here.
def index(request):
	response ['form_berita'] = BeritaForm
	html = 'app_3/berita.html'
	return render(request, html, response)

#fungsi add berita
def addBerita(request):
	if request.method == 'POST':
		form = BeritaForm(request.POST)
		if form.is_valid():
			url = form.cleaned_data["Url"]
			judul = form.cleaned_data["Judul"]
			topik = form.cleaned_data['Topik']
			jumlah_kata = int(form.cleaned_data['JumlahKata'])
			tag = form.cleaned_data['Tag']
			waktu = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
			berita = Berita(url,judul,topik,waktu,waktu,jumlah_kata,0,1)
			berita.save()
			return HttpResponseRedirect('/polling_berita/')
	else:
		form = NameForm()
	return render(request, 'name.html', {'form': form})