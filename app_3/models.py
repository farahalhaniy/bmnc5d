from django.db import models

# Create your models here.

class Berita(models.Model):
    url = models.CharField(primary_key=True, max_length=50)
    judul = models.CharField(max_length=100, blank=True, null=True)
    topik = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    jumlah_kata = models.IntegerField(blank=True, null=True)
    rerata_rating = models.FloatField(blank=True, null=True)
    id_universitas = models.ForeignKey('Universitas', models.DO_NOTHING, db_column='id_universitas', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'berita'

class Tag(models.Model):
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    tag = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'tag'
        unique_together = (('url_berita', 'tag'),)

class Universitas(models.Model):
    id = models.IntegerField(primary_key=True)
    jalan = models.CharField(max_length=100, blank=True, null=True)
    kelurahan = models.CharField(max_length=50, blank=True, null=True)
    provinsi = models.CharField(max_length=50, blank=True, null=True)
    kodepos = models.CharField(max_length=10, blank=True, null=True)
    website = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'universitas'
