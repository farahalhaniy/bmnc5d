from django import forms
from .models import *

class MyModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        text = "University #"+str(obj.id)+" - "+str(obj.website)
        return text

class LoginForm(forms.Form):
    username = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Enter your username',
    }
    password = {
        'type': 'password',
        'class': 'form-control',
        'placeholder': 'Enter your password',
    }
    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=username))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=password))
