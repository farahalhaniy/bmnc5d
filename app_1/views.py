from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .forms import *
from django.db import connection
from django.contrib import messages
# Create your views here.
response={}

def index(request):
    response['tes'] = "Tes"
    response['login_form'] = LoginForm
    return render(request, 'app_1/login.html', response)

@csrf_exempt
def login(request):
    print("masuk form")
    form = LoginForm(data=request.POST)
    if (request.method == 'POST' and form.is_valid()):
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)

        query = "SELECT id FROM narasumber WHERE username = " + "'" + str(username) + "'" + " and password  = " + "'" + str(password) + "'"
        query_uname = "SELECT id FROM narasumber WHERE username = " + "'" + str(username) + "'"
        print(query)
        cursor = connection.cursor()
        cursor.execute(query)

        get_id = cursor.fetchone()
        cursor.execute(query_uname)
        get_nama = cursor.fetchone()
        if(get_id is not None):
            request.session['username'] = username
            request.session['is_login'] = "login"
            request.session['id'] = get_id[0]
            messages.success(request, "Anda berhasil login")
            return HttpResponseRedirect(reverse('apps-public:news-page'))
        elif(get_nama is not None and get_id is None):
            messages.error(request, "Username atau password salah")
            return HttpResponseRedirect(reverse('app_1:login'))
        else:
            messages.error(request, "Anda belum mendaftar")
            return HttpResponseRedirect(reverse('app_1:login'))

def logout(request):
    try:
        request.session.flush()
    except KeyError:
        pass
    messages.info(request, "Anda berhasil logout.")
    return HttpResponseRedirect(reverse('app_1:login'))
