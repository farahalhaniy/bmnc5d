from django.conf.urls import url
from .views import *
from django.contrib.auth import views as auth_views
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^login/$', login, name='login-data'),
    url(r'^logout/$', logout, name='logout'),
]