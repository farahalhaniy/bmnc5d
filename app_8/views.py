from django.shortcuts import render
from django.http import HttpResponseRedirect

response = {}

# Create your views here.
def index(request):
    html = 'app_8/polling.html'
    return render(request, html, response)