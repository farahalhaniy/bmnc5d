# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Berita(models.Model):
    url = models.CharField(primary_key=True, max_length=50)
    judul = models.CharField(max_length=100, blank=True, null=True)
    topik = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    jumlah_kata = models.IntegerField(blank=True, null=True)
    rerata_rating = models.FloatField(blank=True, null=True)
    id_universitas = models.ForeignKey('Universitas', models.DO_NOTHING, db_column='id_universitas', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'berita'


class Dosen(models.Model):
    id_narasumber = models.ForeignKey('Narasumber', models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    nik_dosen = models.CharField(max_length=20, blank=True, null=True)
    jurusan = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dosen'


class Honor(models.Model):
    id_narasumber = models.ForeignKey('Narasumber', models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    tgl_diberikan = models.DateTimeField()
    jumlah_berita = models.IntegerField(blank=True, null=True)
    jumlah_gaji = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'honor'
        unique_together = (('id_narasumber', 'tgl_diberikan'),)


class Komentar(models.Model):
    id = models.IntegerField(primary_key=True)
    tanggal = models.DateTimeField(blank=True, null=True)
    jam = models.TimeField(blank=True, null=True)
    konten = models.CharField(max_length=100, blank=True, null=True)
    nama_user = models.CharField(max_length=50, blank=True, null=True)
    email_user = models.CharField(max_length=50, blank=True, null=True)
    url_user = models.CharField(max_length=50, blank=True, null=True)
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'komentar'


class Kupon(models.Model):
    id = models.IntegerField(primary_key=True)
    tgl_diberikan = models.DateTimeField(blank=True, null=True)
    tgl_kadaluarsa = models.DateTimeField(blank=True, null=True)
    id_narasumber = models.ForeignKey('Narasumber', models.DO_NOTHING, db_column='id_narasumber', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'kupon'


class Mahasiswa(models.Model):
    id_narasumber = models.ForeignKey('Narasumber', models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    npm = models.CharField(max_length=20, blank=True, null=True)
    status = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'mahasiswa'


class Narasumber(models.Model):
    id = models.IntegerField(primary_key=True)
    nama = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    tempat = models.CharField(max_length=50, blank=True, null=True)
    tanggal = models.DateTimeField(blank=True, null=True)
    no_hp = models.CharField(max_length=50, blank=True, null=True)
    id_universitas = models.IntegerField(blank=True, null=True)
    jumlah_berita = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'narasumber'


class NarasumberBerita(models.Model):
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    id_narasumber = models.ForeignKey(Narasumber, models.DO_NOTHING, db_column='id_narasumber')

    class Meta:
        managed = False
        db_table = 'narasumber_berita'
        unique_together = (('url_berita', 'id_narasumber'),)


class Polling(models.Model):
    id = models.IntegerField(primary_key=True)
    polling_start = models.DateTimeField(blank=True, null=True)
    polling_end = models.DateTimeField(blank=True, null=True)
    total_responden = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'polling'


class PollingBerita(models.Model):
    id_polling = models.ForeignKey(Polling, models.DO_NOTHING, db_column='id_polling', primary_key=True)
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'polling_berita'


class PollingBiasa(models.Model):
    id_polling = models.ForeignKey(Polling, models.DO_NOTHING, db_column='id_polling', primary_key=True)
    url = models.CharField(max_length=50, blank=True, null=True)
    deskripsi = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'polling_biasa'


class Rating(models.Model):
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    ip_address = models.CharField(max_length=50)
    nilai = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rating'
        unique_together = (('url_berita', 'ip_address'),)


class Rekening(models.Model):
    nomor = models.CharField(primary_key=True, max_length=20)
    nama_bank = models.CharField(max_length=20, blank=True, null=True)
    id_narasumber = models.ForeignKey(Narasumber, models.DO_NOTHING, db_column='id_narasumber', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rekening'


class Respon(models.Model):
    id_polling = models.ForeignKey(Polling, models.DO_NOTHING, db_column='id_polling', primary_key=True)
    jawaban = models.CharField(max_length=50)
    jumlah_dipilih = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'respon'
        unique_together = (('id_polling', 'jawaban'),)


class Responden(models.Model):
    id_polling = models.ForeignKey(Polling, models.DO_NOTHING, db_column='id_polling', primary_key=True)
    ip_address = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'responden'
        unique_together = (('id_polling', 'ip_address'),)


class Riwayat(models.Model):
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    id_riwayat = models.IntegerField()
    waktu_revisi = models.DateTimeField(blank=True, null=True)
    konten = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'riwayat'
        unique_together = (('url_berita', 'id_riwayat'),)


class Staf(models.Model):
    id_narasumber = models.ForeignKey(Narasumber, models.DO_NOTHING, db_column='id_narasumber', primary_key=True)
    nik_staf = models.CharField(max_length=20, blank=True, null=True)
    posisi = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'staf'


class Tag(models.Model):
    url_berita = models.ForeignKey(Berita, models.DO_NOTHING, db_column='url_berita', primary_key=True)
    tag = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'tag'
        unique_together = (('url_berita', 'tag'),)


class Universitas(models.Model):
    id = models.IntegerField(primary_key=True)
    jalan = models.CharField(max_length=100, blank=True, null=True)
    kelurahan = models.CharField(max_length=50, blank=True, null=True)
    provinsi = models.CharField(max_length=50, blank=True, null=True)
    kodepos = models.CharField(max_length=10, blank=True, null=True)
    website = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'universitas'
