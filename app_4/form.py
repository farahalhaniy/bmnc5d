from django import forms
from .models import *


class BeritaPolForm(forms.Form):

	Url = {
			'type': 'text',
			'class': 'form-control',
			'placeholder': "Insert url",
	}

	start_date = {
	        'type': 'date',
	        'class': 'form-control',
	        'placeholder': "Poll started at",
	}


	end_date = {
	        'type': 'date',
	        'class': 'form-control',
	        'placeholder': "Poll ended at",

	}    

	Url =  forms.CharField(label='Url Berita', required=True, max_length=50, widget=forms.TextInput(attrs=Url))
	StartDate =  forms.DateTimeField(label='Start Date', required=True, widget=forms.DateInput(attrs=start_date))
	EndDate =  forms.DateTimeField(label='End Date', required=True, widget=forms.DateInput(attrs=end_date))
