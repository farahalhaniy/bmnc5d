from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .form import *
from django.db import connection
from django.contrib import messages
from datetime import datetime

response = {}

# Create your views here.
def index(request):
	response ['form_pol'] = BeritaPolForm
	html = 'app_4/polling_berita.html'
	return render(request, html, response)

# add polling
def addPolBerita(request):
	if request.method == 'POST':
		form = BeritaPolForm(request.POST)
		if form.is_valid():	
			return HttpResponseRedirect('/polling_biasa/')
	else:
		form = NameForm()
	return render(request, 'name.html', {'form': form})