from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import *
from .form import *
from django.db import connection
from django.contrib import messages
from datetime import datetime


response = {}

# Create your views here.
def index(request):
	response ['form_pol'] = BiasaPolForm
	html = 'app_5/polling_biasa.html'
	return render(request, html, response)

# add polling
def addPolBiasa(request):
	if request.method == 'POST':
		form = BiasaPolForm(request.POST)
		if form.is_valid():	
			return HttpResponseRedirect('/addberita/')
	else:
		form = NameForm()
	return render(request, 'name.html', {'form': form})
