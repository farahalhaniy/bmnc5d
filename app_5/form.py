from django import forms
from .models import *


class BiasaPolForm(forms.Form):

	Deskripsi = {
			'type': 'text',
			'class': 'form-control',
			'placeholder': "Deskripsi Polling",
	}

	start_date = {
	        'type': 'date',
	        'class': 'form-control',
	        'placeholder': "Poll started at",
	}


	end_date = {
	        'type': 'date',
	        'class': 'form-control',
	        'placeholder': "Poll ended at",

	}    

	Deskripsi =  forms.CharField(label='Deskripsi', required=True, max_length=50, widget=forms.TextInput(attrs=Deskripsi))
	StartDate =  forms.DateTimeField(label='Start Date', required=True, widget=forms.DateInput(attrs=start_date))
	EndDate =  forms.DateTimeField(label='End Date', required=True, widget=forms.DateInput(attrs=end_date))
