from django.conf.urls import url, include
from django.contrib import admin
import app_6.urls as app_6
import app_7.urls as app_7
import app_8.urls as app_8
import app_1.urls as app_1
import app_2.urls as app_2
import app_3.urls as app_3
import app_4.urls as app_4
import app_5.urls as app_5

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^profile/', include(app_6)),
    url(r'^newsbyuser/', include(app_7)),
    url(r'^polling/', include(app_8)),
    url(r'^login/', include(app_1)),   
    url(r'^register/', include(app_2)), 
    url(r'^addBerita/', include(app_3)), 
    url(r'^polling_berita/', include(app_4)),
    url(r'^polling_biasa/', include(app_5)),
]